/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.lynxlist.test;

import org.lynxdb.lynxlist.structure.LynxList;

/**
 *
 * @author martin
 */
public class ThreadMachine {
    private final LynxList<Thread> threads;

    public ThreadMachine() {
        threads = new LynxList<>();
    }
    
    public void addThread(Runnable r){
        Thread t = new Thread(r);
        threads.add(t);
        threads.getLast().start();
    }
    
    public void stopAll(){
        while(!threads.isEmpty()){
            try {
                threads.poll().stop();
            } catch (Exception e) {
            }
        }
    }
    
}

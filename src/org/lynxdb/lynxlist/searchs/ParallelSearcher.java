/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.lynxlist.searchs;

import java.util.function.Predicate;
import org.lynxdb.lynxlist.structure.LynxList;

/**
 *
 * @author martin
 */
public class ParallelSearcher {
    
    public static <T> LynxList<T> search(LynxList<T> list, Predicate<? super T> condition){
        LynxList<T> listaResultados = new LynxList<>();
        TSearcher<T> searcher1 = new TSearcher<>(list, listaResultados, condition, false);
        TSearcher<T> searcher2 = new TSearcher<>(list, listaResultados, condition, true);
        
        while (!searcher1.isFinished() && !searcher2.isFinished()) {}
    
        return listaResultados;
    } 
}

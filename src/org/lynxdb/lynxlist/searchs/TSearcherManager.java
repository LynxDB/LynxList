/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.lynxlist.searchs;

import java.util.function.Predicate;
import java.util.stream.Stream;
import org.lynxdb.lynxlist.structure.LynxList;

/**
 *
 * @author martin
 */
public class TSearcherManager<T> {
    private final TSearcher<T> searcher1;
    private final TSearcher<T> searcher2;
    private final LynxList<T> listResults;

    public TSearcherManager(LynxList<T> listSearch, Predicate<? super T> predicate) {
        listResults = new LynxList<>();
        searcher1 = new TSearcher<>(listSearch, listResults, predicate);
        searcher2 = new TSearcher<>(listSearch, listResults, predicate, true);
    }
    
    private void stopSearchers(){
        searcher1.interrupt();
        searcher2.interrupt();
    }
    
    public T getFirstOcurrence(){
        while (!searcher1.isFinished() || !searcher2.isFinished() && 
                listResults.isEmpty()){}
        
        /*
            Hay problemas con el findAny
            Nuevo
            while (listResults.isEmpty() &&
                (!searcher1.isFinished() || !searcher2.isFinished())){}
        */
        return listResults.pollFirst();
    }
    
    public LynxList<T> getResults(){
        while (!searcher1.isFinished() && !searcher2.isFinished()) {}
        return listResults;
    }
    
    public Stream<T> getStreamResults(){
        return getResults().stream();
    }
    
}
